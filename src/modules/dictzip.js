/**
 * Copyright (c) 2024 Thomas Vogt
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import { DictZipFileBase } from "./common.js";

class DictZipFile extends DictZipFileBase {
  load() {
    return new Promise((resolve, reject) => {
      let reader = new FileReader();
      reader.onload = (evt) => {
        try {
          this.getChunks(evt.target.result);
          resolve();
        } catch (err) {
          reject(err);
        }
      };
      reader.readAsArrayBuffer(this.dzfile);
    });
  }

  read(pos, len) {
    return new Promise((resolve, reject) => {
      if (!this.verified) {
        reject(new Error("Read attempt before loadend."));
      }

      let { firstchunk, lastchunk, offset, finish, dzfileslice } =
        this.prepareRead(pos, len);

      let reader = new FileReader();
      reader.onload = (evt) => {
        try {
          const inflated = this.inflateChunks(
            evt.target.result,
            firstchunk,
            lastchunk,
          );
          resolve(inflated.slice(offset, finish));
        } catch (err) {
          reject(err);
        }
      };
      reader.readAsArrayBuffer(dzfileslice);
    });
  }
}

export default DictZipFile;
