/**
 * Copyright (c) 2024 Thomas Vogt
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import pako from "pako";

export function intArrayToString(arr) {
  let ret = "";
  for (let i = 0; i < arr.length; i++) {
    ret += String.fromCharCode(arr[i]);
  }
  return ret;
}

function mergeArrayBuffers(buffer1, buffer2) {
  let tmp = new Uint8Array(buffer1.byteLength + buffer2.byteLength);
  tmp.set(new Uint8Array(buffer1), 0);
  tmp.set(new Uint8Array(buffer2), buffer1.byteLength);
  return tmp.buffer;
}

function readZeroTerminatedString(buffer, offset) {
  let result = "";
  for (let n = 1; true; n++) {
    offset = offset - result.length;
    let view = new Uint8Array(buffer.slice(offset, offset + n * 1024)),
      end = Array.prototype.indexOf.call(view, 0);
    if (end == -1) {
      end = view.length;
      if (end == 0) throw new Error("Unexpected end of buffer");
    }
    result += intArrayToString(view.subarray(0, end));
    if (end < view.length) break;
  }
  return result;
}

function readGzipHeader(buffer) {
  let FTEXT = 1,
    FHCRC = 2,
    FEXTRA = 4,
    FNAME = 8,
    FCOMMENT = 16;

  let position = 0,
    view = new Uint8Array(buffer, position, 10),
    headerData = {
      ID1: 0,
      ID2: 0,
      CM: 0,
      FLG: 0,
      MTIME: 0,
      XFL: 0,
      OS: 0,
      FEXTRA: {
        XLEN: 0,
        SUBFIELDS: [],
      },
      FNAME: "",
      FCOMMENT: "",
      FHCRC: "",
    };

  if (view[0] != 0x1f || view[1] != 0x8b) {
    throw new Error("Not a gzip header.");
  }

  headerData["ID1"] = view[0];
  headerData["ID2"] = view[1];
  headerData["CM"] = view[2];
  headerData["FLG"] = view[3];
  headerData["MTIME"] = view[4] << 0;
  headerData["MTIME"] |= view[5] << 8;
  headerData["MTIME"] |= view[6] << 16;
  headerData["MTIME"] |= view[7] << 24;
  headerData["XFL"] = view[8];
  headerData["OS"] = view[9];
  position += 10;

  // FEXTRA
  if ((headerData["FLG"] & FEXTRA) != 0x00) {
    view = new Uint16Array(buffer, position, 2);
    headerData["FEXTRA"]["XLEN"] = view[0];
    position += 2;

    // FEXTRA SUBFIELDS
    view = new Uint8Array(buffer, position, headerData["FEXTRA"]["XLEN"]);
    while (true) {
      let len = view[2] + 256 * view[3];
      let subfield = {
        SI1: String.fromCharCode(view[0]),
        SI2: String.fromCharCode(view[1]),
        LEN: len,
        DATA: view.subarray(4, 4 + len),
      };
      headerData["FEXTRA"]["SUBFIELDS"].push(subfield);
      view = view.subarray(4 + len);
      if (view.length == 0) break;
    }
    position += headerData["FEXTRA"]["XLEN"];
  }

  // FNAME
  if ((headerData["FLG"] & FNAME) != 0x00) {
    headerData["FNAME"] = readZeroTerminatedString(buffer, position);
    position += headerData["FNAME"].length;
  }

  // FCOMMENT
  if ((headerData["FLG"] & FCOMMENT) != 0x00) {
    headerData["FCOMMENT"] = readZeroTerminatedString(buffer, position);
    length += headerData["FCOMMENT"].length;
  }

  // FHCRC
  if ((headerData["FLG"] & FHCRC) != 0x00) {
    view = new Uint16Array(buffer, position, 2);
    headerData["FHCRC"] = view[0];
    position += 2;
  }

  headerData["LENGTH"] = position + 1;
  return headerData;
}

export class DictZipFileBase {
  constructor(dzfile) {
    this.dzfile = dzfile;
    this.verified = false;
    this.gzipHeader = undefined;
    this.ver = undefined;
    this.chlen = 0;
    this.chcnt = 0;
    this.chunks = [];
  }

  getChunks(buffer) {
    this.gzipHeader = readGzipHeader(buffer);
    let subfields = this.gzipHeader["FEXTRA"]["SUBFIELDS"],
      found = false,
      sf;
    for (let i = 0; i < subfields.length; i++) {
      sf = subfields[i];
      if (sf["SI1"] == "R" || sf["SI2"] == "A") {
        found = true;
        break;
      }
    }
    if (!found) {
      throw new Error("Not a dictzip header.");
    } else {
      let b = sf["DATA"];
      this.ver = b[0] + 256 * b[1];
      this.chlen = b[2] + 256 * b[3];
      this.chcnt = b[4] + 256 * b[5];
      for (let i = 0, chpos = 0; i < this.chcnt && 2 * i + 6 < b.length; i++) {
        let tmpChLen = b[2 * i + 6] + 256 * b[2 * i + 7];
        this.chunks.push([chpos, tmpChLen]);
        chpos += tmpChLen;
      }
      this.verified = true;
      return true;
    }
  }

  prepareRead(pos, len) {
    if (typeof pos === "undefined") pos = 0;
    if (typeof len === "undefined") len = this.chlen * this.chunks.length;

    let firstchunk = Math.min(
        Math.floor(pos / this.chlen),
        this.chunks.length - 1,
      ),
      lastchunk = Math.min(
        Math.floor((pos + len) / this.chlen),
        this.chunks.length - 1,
      ),
      offset = pos - firstchunk * this.chlen,
      finish = offset + len,
      dzfileslice = this.dzfile.slice(
        this.gzipHeader["LENGTH"] + this.chunks[firstchunk][0],
        this.gzipHeader["LENGTH"] +
          this.chunks[lastchunk][0] +
          this.chunks[lastchunk][1],
      );

    return { firstchunk, lastchunk, offset, finish, dzfileslice };
  }

  inflateChunks(buffer, firstchunk, lastchunk) {
    let outBuffer = new ArrayBuffer(0);
    for (
      let i = firstchunk, j = 0;
      i <= lastchunk && j < buffer.byteLength;
      j += this.chunks[i][1], i++
    ) {
      let chunk = buffer.slice(j, j + this.chunks[i][1]);
      const inflated = pako.inflateRaw(chunk);
      outBuffer = mergeArrayBuffers(outBuffer, inflated);
    }
    return outBuffer;
  }

  read(pos, len) {
    throw new Error("read method not implemented.");
  }
}
