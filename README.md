# dictzip.js

JavaScript module for handling dictzip compressed files effectively, i.e. it does not
uncompress and load into memory the whole data blob, but instead provides an interface
for (asynchronous) random access to the compressed data.

Hence dictzip.js can handle really huge amounts of data which may occur e.g. when working with
local files accessed through the W3C's File API.

This implementation internally works with ArrayBuffers and will return data
as ArrayBuffers and thus return results comparable to the method
`readAsArrayBuffer` of the `FileReader` object in the File API. For reading
e.g. UTF-8 encoded text you may still have to convert the resulting
ArrayBuffer to a UTF-8 string for human readable output.

dictzip.js depends on the inflate implementation in [pako](https://github.com/nodeca/pako)
version 1 because version 2 changed the handling of raw input
(see [nodeca/pako#235](https://github.com/nodeca/pako/issues/235)).

## Documentation

### Build and usage

Bundle this project as a single file in `./dist/dictzip.js` as follows:

    $ npm install
    $ npm run build

Use an ES6 import statement to add the class `DictZipFile` to your scope:

    import { DictZipFile } from 'dist/dictzip.js';

Create a new instance by specifying a `File` (or `Blob`) object and initialize asynchronously:

    let dzreader = new DictZipFile(blob);
    await dzreader.load();

The `load` method reads the file's header and verifies that it's indeed a dictzip file. After
initialization, the main interface is the `read` method:

    let buffer = await dzreader.read(offset, size);

The unsigned integers `offset` and `size` represent the offset and size with respect to the inflated
data. Both are optional and default to 0 (for `offset`) and the inflated data's bytelength (for `size`).
The function returns an ArrayBuffer `buffer` containing the inflated data.

### Synchronous interface

Furthermore, the project provides an (untested) synchronous API for use inside of web workers.
Import it into your worker's scope and use it as follows:

    import { DictZipFileSync } from 'dist/dictzip.js';
    let dzreader = new DictZipFileSync(blob);
    let buffer = dzreader.read(offset, size);

The `load` method from the synchronous case is obsolete since the respective checks are done in the
constructor.

### Examples

To run the examples in the `./demo/` directory, you first need to run `npm run build`. After that,
use `npm run demo:async` and `npm run demo:sync` to run the example using the asynchronous or
synchronous API, respectively (a browser window should open automatically).

## Further reading

- Format documentation (dictzip manpage): http://linux.die.net/man/1/dictzip
- gzip format documentation (referenced in dictzip documentation): http://www.gzip.org/zlib/rfc-gzip.html
- Original implementation (written in C): http://dict.cvs.sourceforge.net/viewvc/dict/dictd1/dictzip.c?view=markup
- Python lib: http://code.google.com/p/pytoolkits/source/browse/trunk/utils/stardict/dictzip.py
- Java lib: http://code.google.com/p/toolkits/source/browse/trunk/android/YAStarDict/src/com/googlecode/toolkits/stardict/DictZipFile.java
- JavaScript library for StarDict: https://framagit.org/tuxor1337/stardict.js
